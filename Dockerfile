FROM node:18.8.0-alpine3.16 as builder
WORKDIR /app
COPY package*.json ./
COPY . .
RUN npm install --include=dev \
    npm cache clean -f
RUN  npm run 

FROM node:18.8.0-alpine3.16
WORKDIR /app
COPY --from=builder /app/ ./
EXPOSE 3000
CMD [ "node", "src/index.js" ]