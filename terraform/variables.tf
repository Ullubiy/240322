variable "zone" {
  default = "ru-central1-a"
}

variable "platform_id" {
  default = "standard-v1"
}

variable "image_id" {
  default = "fd85u0rct32prepgjlv0"
}

variable "v4_cidr_blocks" {
  default = ["10.5.0.0/24"]
}

variable "resources" {
  default = {
    cores  = 2
    memory = 4
  }
}
