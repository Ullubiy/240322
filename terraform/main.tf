


###########################################dev01##################################################

resource "yandex_compute_instance" "dev01" {
  name        = "dev01"
  platform_id = var.platform_id
  zone        = var.zone

  resources {
    cores  = var.resources.cores
    memory = var.resources.memory


  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.network.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"

  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/id_ed25519")
    host        = self.network_interface[0].nat_ip_address

  }
  provisioner "file" {
    source      = "/home/server/240322/Light/ansible/playbook.yml"
    destination = "/home/ubuntu/playbook.yml"
  }
  provisioner "file" {
    source      = "/home/server/240322/Light/ansible/requirements.yml"
    destination = "/home/ubuntu/requirements.yml"
  }

  provisioner "file" {
    content     = <<EOF
      #! /bin/bash
      ip_address="${self.network_interface[0].nat_ip_address}"
      hostname="dev.example.com"
      echo "$ip_address   $hostname" >> /etc/hosts
      EOF
    destination = "/home/ubuntu/addshost.sh"
  }

  provisioner "remote-exec" {
    inline = [
      # "sudo rm /var/lib/apt/lists/* -vf",
      # "sudo apt-get clean",
      # "sudo apt-get upgrade -y",
      # "sudo apt-add-repository ppa:ansible/ansible -y",
      # "sudo apt-get update",
      "sudo apt install python3 -y",
      "sudo apt install -y ansible",
      "sudo hostnamectl set-hostname dev01",
      "ansible-galaxy install -f -r ./requirements.yml",
      "ansible-playbook --connection=local --inventory 127.0.0.1 -u ubuntu  ./playbook.yml",
      "sudo chmod +x /home/ubuntu/addshost.sh",
      "sudo bash /home/ubuntu/addshost.sh"


    ]

  }
}
###########################################network##################################################

resource "yandex_vpc_network" "network" {
  name = "network"
}

resource "yandex_vpc_subnet" "network" {
  zone           = var.zone
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = var.v4_cidr_blocks
}

###########################################prod01##################################################

resource "yandex_compute_instance" "prod01" {
  name        = "prod01"
  platform_id = var.platform_id
  zone        = var.zone

  resources {
    cores  = var.resources.cores
    memory = var.resources.memory
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = 15
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.network.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
    # user-data = "${file("./install_prod.sh")}"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/id_ed25519")
    host        = self.network_interface[0].nat_ip_address

  }
  provisioner "file" {
    source      = "/home/server/240322/Light/ansible/playbook.yml"
    destination = "./playbook.yml"
  }
  provisioner "file" {
    source      = "/home/server/240322/Light/ansible/requirements.yml"
    destination = "./requirements.yml"
  }

  provisioner "file" {
    content     = <<EOF
      #! /bin/bash
      ip_address="${self.network_interface[0].nat_ip_address}"
      hostname="example.com"
      echo "$ip_address   $hostname" >> /etc/hosts
      EOF
    destination = "/home/ubuntu/addshost.sh"
  }

  provisioner "remote-exec" {
    inline = [
      # "sudo rm /var/lib/apt/lists/* -vf",
      # "sudo apt-get clean",
      # "sudo apt-add-repository ppa:ansible/ansible -y",
      # "sudo apt-get update",
      # "sudo apt-get upgrade -y",
      "sudo apt install python3 -y",
      "sudo apt install -y ansible",
      "sudo hostnamectl set-hostname prod01",
      "ansible-galaxy install -f -r ./requirements.yml",
      "ansible-playbook --connection=local --inventory 127.0.0.1 -u ubuntu  ./playbook.yml",
      "sudo chmod +x /home/ubuntu/addshost.sh",
      "sudo bash /home/ubuntu/addshost.sh"


    ]

  }
}







