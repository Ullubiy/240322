#!/bin/bash
sudo apt update && sleep 5;
sudo apt install -y git;
sudo apt install python3 -y;
sudo apt install -y ansible;
git clone https://gitlab.com/Ullubiy/240322.git;
cd 240322;
sudo hostnamectl set-hostname dev01;
ip_address="${self.network_interface[0].nat_ip_address}"
hostname="dev.example.com"
sudo echo "$ip_address   $hostname" >> /etc/hosts
ansible-galaxy install -f -r ansible/requirements.yml;
ansible-playbook --connection=local --inventory 127.0.0.1 -u ubuntu  ansible/playbook.yml;
# sudo chmod +x /home/ubuntu/addshost.sh;
# sudo bash /home/ubuntu/addshost.sh
